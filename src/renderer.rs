use sdl2;
use gl;
use std;
use std::ffi::{CString, CStr};
use glwrapper::*;
use cgmath::*;
use stb_image::*;

const MAX_COMMANDS : usize = 128;
const VERTICES: [f32; 30] = [
    -0.5, -0.5, 0.0, 0.0, 1.0,
    -0.5,  0.5, 0.0, 0.0, 0.0,
     0.5,  0.5, 0.0, 1.0, 0.0,
     0.5,  0.5, 0.0, 1.0, 0.0,
     0.5, -0.5, 0.0, 1.0, 1.0,
    -0.5, -0.5, 0.0, 0.0, 1.0
];

#[derive(Copy, Clone)]
pub enum RenderCommandType {
    Unknown,
    Quad,
    Sprite,
    AnimatedSprite
}

#[derive(Copy, Clone)]
pub enum Color {
    Rgba(f32, f32, f32, f32),
    Rgb(f32, f32, f32)
}

#[derive(Copy, Clone)]
pub struct Texture {
    pub id: gl::types::GLuint,
    pub width: usize,
    pub height: usize
}

#[derive(Copy, Clone)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32
}

#[derive(Copy, Clone)]
pub struct AnimationFrame {
    pub rect: Rect,
    pub time: f64
}

impl AnimationFrame {
    pub fn new(x: i32, y: i32, width: i32, height: i32, time: f64) -> AnimationFrame {
        AnimationFrame { rect: Rect { x: x, y: y, width: width, height: height }, time: time }
    }
}

#[derive(Copy, Clone)]
pub struct AnimationHandle {
    pub handle: usize
}

pub struct Animation {
    pub name: String,
    pub texture: Texture,
    frames: Vec<AnimationFrame>,
    pub is_playing: bool,
    loopable: bool,
    pub current_time: f64,
    current_frame: usize
}

impl Animation {
    pub fn new(name: String, texture: Texture, is_playing: bool, loopable: bool) -> Animation {
        Animation { name: name, texture: texture, frames: Vec::new(), is_playing: is_playing, loopable: loopable, current_time: 0.0, current_frame: 0 }
    }

    pub fn add_frame(&mut self, frame: AnimationFrame) {
        self.frames.push(frame);
    }

    pub fn current_frame(&self) -> Option<AnimationFrame> {
        if self.frames.len() > 0 {
            return Some(self.frames[0]);
        }

        None
    }
}

#[derive(Copy, Clone)]
pub struct QueuedRenderCommand {
    pub command_type: RenderCommandType,
    pub position: [f32; 3],
    pub scale: [f32; 3],
    pub color: [f32; 4],
    pub texture: Option<Texture>,
    pub animation_handle: Option<AnimationHandle>
}

pub struct Camera {
    pub position: Vector3<f32>,
    pub rotation: Vector3<f32>,
    view_matrix: Matrix4<f32>,
    needs_update: bool
}

impl Camera {
    pub fn update_position(&mut self, x: f32, y: f32, z: f32) {
        self.position = Vector3 { x: x, y: y, z: z };
        self.needs_update = true;
    }
    pub fn camera_view_matrix(&mut self) -> Matrix4<f32> {
        if self.needs_update {
            self.view_matrix = Matrix4::from_translation(Vector3 { x: -self.position.x, y: -self.position.y, z: self.position.z });
            self.needs_update = false;
        }
        self.view_matrix
    }
}

pub struct Renderer {
    pub close: bool,
    window_size: (u32, u32),
    _context: sdl2::video::GLContext,
    window: sdl2::video::Window,
    quad_vao: gl::types::GLuint,
    quad_vbo: gl::types::GLuint,
    quad_shader_program: Program,
    spritesheet_shader_program: Program,
    queued_commands: [QueuedRenderCommand; MAX_COMMANDS],
    queued_command_count: usize,
    animations: Vec<Animation>,
    pub clear_color: Color,
    pub camera: Camera
}

impl Renderer {
    pub fn create(video_subsystem: sdl2::VideoSubsystem, window_width: u32, window_height: u32) -> Renderer {
        video_subsystem.gl_set_swap_interval(1);

        let gl_attr = video_subsystem.gl_attr();
        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(3, 3);

        let window = video_subsystem
            .window("Rustified GLITCHHEART", window_width, window_height)
            .opengl()
            .build()
            .unwrap();

        let gl_context = window.gl_create_context().unwrap();
        let _gl = gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

        let mut vbo: gl::types::GLuint = 0;
        let mut vao: gl::types::GLuint = 0;
        use std::ffi::CString;

        let quad_vert_shader = Shader::from_vert_source(
            &CString::new(include_str!("shaders/quad.vert")).unwrap()
            ).unwrap();

        let quad_frag_shader = Shader::from_frag_source(
            &CString::new(include_str!("shaders/quad.frag")).unwrap()
            ).unwrap();

        let quad_shader_program = Program::from_shaders(&[quad_vert_shader, quad_frag_shader]).unwrap();
        quad_shader_program.set_used();

        let spritesheet_vert_shader = Shader::from_vert_source(
            &CString::new(include_str!("shaders/spritesheet_quad.vert")).unwrap()
            ).unwrap();

        let spritesheet_frag_shader = Shader::from_frag_source(
            &CString::new(include_str!("shaders/spritesheet_quad.frag")).unwrap()
            ).unwrap();

        let spritesheet_shader_program = Program::from_shaders(&[spritesheet_vert_shader, spritesheet_frag_shader]).unwrap();
        spritesheet_shader_program.set_used();

        // vertex data
        gl_gen_buffers(1, &mut vbo);

        unsafe {
            gl_bind_buffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(gl::ARRAY_BUFFER, (VERTICES.len() * std::mem::size_of::<f32>()) as gl::types::GLsizeiptr, VERTICES.as_ptr() as *const gl::types::GLvoid, gl::STATIC_DRAW);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        }

        unsafe {
            gl::GenVertexArrays(1, &mut vao);
        }
        unsafe {
            gl::BindVertexArray(vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::EnableVertexAttribArray(0); // this is "layout (location = 0)" in vertex shader
            gl::VertexAttribPointer(
                0, // index of the generic vertex attribute ("layout (location = 0)")
                3, // the number of components per generic vertex attribute
                gl::FLOAT, // data type
                gl::FALSE, // normalized (int-to-float conversion)
                (5 * std::mem::size_of::<f32>()) as gl::types::GLint, // stride (byte offset between consecutive attributes)
                std::ptr::null() // offset of the first component
                );
            gl::EnableVertexAttribArray(1); // this is "layout (location = 1)" in vertex shader
            gl::VertexAttribPointer(
                1, // index of the generic vertex attribute ("layout (location = 0)")
                2, // the number of components per generic vertex attribute
                gl::FLOAT, // data type
                gl::FALSE, // normalized (int-to-float conversion)
                (5 * std::mem::size_of::<f32>()) as gl::types::GLint, // stride (byte offset between consecutive attributes)
                (3 * std::mem::size_of::<f32>()) as *const gl::types::GLvoid // offset of the first component
                );
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }

        Renderer {
            close: false,
            window_size: (window_width, window_height),
            _context: gl_context,
            window: window,
            quad_vao: vao,
            quad_vbo: vbo,
            quad_shader_program: quad_shader_program,
            spritesheet_shader_program: spritesheet_shader_program,
            queued_commands: [QueuedRenderCommand { command_type: RenderCommandType::Unknown, position: [0.0; 3], scale: [0.0; 3], color: [0.0; 4], texture: None, animation_handle: None }; MAX_COMMANDS],
            queued_command_count: 0,
            animations: Vec::new(),
            camera: Camera { position: Vector3::new(0.0, 0.0, 0.0), rotation: Vector3::new(0.0, 0.0, 0.0), needs_update: true, view_matrix: Matrix4::identity() },
            clear_color: Color::Rgba(0.0, 0.0, 0.0, 1.0) }
    }

    pub fn register_animation(&mut self, animation: Animation) -> AnimationHandle {
        let handle = self.animations.len();
        self.animations.push(animation);
        AnimationHandle { handle: handle }
    }

    pub fn load_texture(path: &str) -> Result<Texture, std::string::String> {
        let mut texture_id : gl::types::GLuint = 0;
        let mut width : usize = 0;
        let mut height : usize = 0;

        let load_result = image::load(path);
        match load_result {
            image::LoadResult::Error(message) => {
                return Err(message);
            },
            image::LoadResult::ImageU8(image_data) => {
                width = image_data.width;
                height = image_data.height;
                unsafe {
                    gl::GenTextures(1, &mut texture_id);
                    gl::BindTexture(gl::TEXTURE_2D, texture_id);
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
                    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);

                    gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as gl::types::GLint, image_data.width as i32, image_data.height as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE,
                		image_data.data.as_ptr() as *const std::os::raw::c_void);
                }
            },
            image::LoadResult::ImageF32(_image_data) => {
            }
        }
        Ok(Texture { id: texture_id, width: width, height: height })
    }

    pub fn push_command(&mut self, command: QueuedRenderCommand) {
        self.queued_commands[self.queued_command_count] = command;
        self.queued_command_count += 1;
    }

    pub fn push_quad_command(&mut self, x: f32, y: f32, width: f32, height: f32, color: [f32; 4]) {
        self.push_command(QueuedRenderCommand { command_type: RenderCommandType::Quad, position: [x, y, 0.0], scale: [width, height, 0.0], color: color, texture: Some(Texture { id: 0, width: 0, height: 0 }), animation_handle: None })
    }

    pub fn push_sprite_command(&mut self, texture: Texture, x: f32, y: f32, width: f32, height: f32, color: [f32; 4]) {
        self.push_command(QueuedRenderCommand { command_type: RenderCommandType::Sprite, position: [x, y, 0.0], scale: [width, height, 0.0], color: color, texture: Some(texture), animation_handle: None })
    }

    pub fn push_animated_sprite_command(&mut self, animation_handle: AnimationHandle, x: f32, y: f32, width: f32, height: f32, color: [f32; 4]) {
        self.push_command(QueuedRenderCommand { command_type: RenderCommandType::AnimatedSprite, position: [x, y, 0.0], scale: [width, height, 0.0], color: color, texture: None, animation_handle: Some(animation_handle) })
    }

    fn render_quad(&mut self, command: QueuedRenderCommand) {
        gl_bind_vertex_array(self.quad_vao);
        gl_bind_buffer(gl::ARRAY_BUFFER, self.quad_vbo);

        self.quad_shader_program.set_used();

        let mut model : Matrix4<f32> = Matrix4::identity().into();
        model = model * Matrix4::from_translation(Vector3 { x: command.position[0], y: command.position[1], z: command.position[2] });
        model = model * Matrix4::from_nonuniform_scale(command.scale[0], command.scale[1], command.scale[0]);

        let projection = ortho(0.0, 1280.0, 0.0, 720.0, -500.0, 500.0);
        let view_matrix = self.camera.camera_view_matrix();

        gl_uniform_mat4fv(gl_get_uniform_location(self.quad_shader_program.id, "projection"), projection);
        gl_uniform_mat4fv(gl_get_uniform_location(self.quad_shader_program.id, "view"), view_matrix);
        gl_uniform_mat4fv(gl_get_uniform_location(self.quad_shader_program.id, "model"), model);

        gl_uniform4f(gl_get_uniform_location(self.quad_shader_program.id, "color"), command.color[0], command.color[1], command.color[2], command.color[3]);

        gl_draw_arrays(gl::TRIANGLES, 0, 6);
        gl_bind_vertex_array(0);
    }

    fn render_sprite(&mut self, command: QueuedRenderCommand) {
        gl_bind_vertex_array(self.quad_vao);
        gl_bind_buffer(gl::ARRAY_BUFFER, self.quad_vbo);

        self.quad_shader_program.set_used();

        let mut model : Matrix4<f32> = Matrix4::identity().into();
        model = model * Matrix4::from_translation(Vector3 { x: command.position[0], y: command.position[1], z: command.position[2] });
        model = model * Matrix4::from_nonuniform_scale(command.scale[0], command.scale[1], command.scale[0]);

        let projection = ortho(0.0, 1280.0, 0.0, 720.0, -500.0, 500.0);
        let view_matrix = self.camera.camera_view_matrix();

        gl_uniform_mat4fv(gl_get_uniform_location(self.quad_shader_program.id, "projection"), projection);
        gl_uniform_mat4fv(gl_get_uniform_location(self.quad_shader_program.id, "view"), view_matrix);
        gl_uniform_mat4fv(gl_get_uniform_location(self.quad_shader_program.id, "model"), model);

        gl_uniform4f(gl_get_uniform_location(self.quad_shader_program.id, "color"), command.color[0], command.color[1], command.color[2], command.color[3]);

        if let Some(texture) = command.texture {
            unsafe {
                gl::ActiveTexture(gl::TEXTURE0);
                gl::Uniform1i(gl_get_uniform_location(self.quad_shader_program.id, "tex"), 0);
                gl::BindTexture(gl::TEXTURE_2D, texture.id);
            }
        }

        gl_draw_arrays(gl::TRIANGLES, 0, 6);
        gl_bind_vertex_array(0);
    }

    fn render_animated_sprite(&mut self, command: QueuedRenderCommand) {
        gl_bind_vertex_array(self.quad_vao);
        gl_bind_buffer(gl::ARRAY_BUFFER, self.quad_vbo);

        self.spritesheet_shader_program.set_used();

        let mut model : Matrix4<f32> = Matrix4::identity().into();
        model = model * Matrix4::from_translation(Vector3 { x: command.position[0], y: command.position[1], z: command.position[2] });
        model = model * Matrix4::from_nonuniform_scale(command.scale[0], command.scale[1], command.scale[0]);

        let projection = ortho(0.0, 1280.0, 0.0, 720.0, -500.0, 500.0);
        let view_matrix = self.camera.camera_view_matrix();

        gl_uniform_mat4fv(gl_get_uniform_location(self.spritesheet_shader_program.id, "projection"), projection);
        gl_uniform_mat4fv(gl_get_uniform_location(self.spritesheet_shader_program.id, "view"), view_matrix);
        gl_uniform_mat4fv(gl_get_uniform_location(self.spritesheet_shader_program.id, "model"), model);

        gl_uniform4f(gl_get_uniform_location(self.spritesheet_shader_program.id, "color"), command.color[0], command.color[1], command.color[2], command.color[3]);

        if let Some(animation_handle) = command.animation_handle {
            let animation = &self.animations[animation_handle.handle];
            let frame = animation.frames[animation.current_frame];
            //println!("Sizes {} {} {} {} {} {}", animation.texture.width, animation.texture.height, frame.rect.width, frame.rect.height, frame.rect.x, frame.rect.y);
            gl_uniform2f(gl_get_uniform_location(self.spritesheet_shader_program.id, "texture_size"), animation.texture.width as f32, animation.texture.height as f32);
            gl_uniform2f(gl_get_uniform_location(self.spritesheet_shader_program.id, "frame_size"), frame.rect.width as f32, frame.rect.height as f32);
            gl_uniform2f(gl_get_uniform_location(self.spritesheet_shader_program.id, "sprite_position"), frame.rect.x as f32, frame.rect.y as f32);

            unsafe {
                gl::ActiveTexture(gl::TEXTURE0);
                gl::Uniform1i(gl_get_uniform_location(self.spritesheet_shader_program.id, "tex"), 0);
                gl::BindTexture(gl::TEXTURE_2D, animation.texture.id);
            }
        }

        gl_draw_arrays(gl::TRIANGLES, 0, 6);
        gl_bind_vertex_array(0);
    }

    pub fn update_animations(&mut self, dt: f64) {
        for animation in &mut self.animations {
            if animation.is_playing && animation.frames.len() > 0 {
                let frame = animation.frames[animation.current_frame];
                if animation.current_time >= frame.time {
                    animation.current_frame = animation.current_frame + 1;
                    if animation.current_frame >= animation.frames.len() {
                        animation.current_frame = 0;
                        animation.is_playing = animation.loopable;
                    }
                    animation.current_time = 0.0;
                }

                animation.current_time = animation.current_time + dt;
            }
        }
    }

    pub fn render(&mut self) {
        gl_viewport(0, 0, self.window_size.0 as i32, self.window_size.1 as i32);
        gl_clear_color(self.clear_color);
        gl_clear(gl::COLOR_BUFFER_BIT);

        for i in 0..self.queued_command_count {
            let command = self.queued_commands[i];
            match command.command_type {
                RenderCommandType::Quad => {
                    self.render_quad(command);
                },
                RenderCommandType::Sprite => {
                    self.render_sprite(command);
                },
                RenderCommandType::AnimatedSprite => {
                    self.render_animated_sprite(command);
                },
                RenderCommandType::Unknown => {

                }
            }
        }

        self.window.gl_swap_window();

        self.queued_command_count = 0;
    }
}

// Structs
pub struct Shader {
    pub id: gl::types::GLuint,
}

impl Shader {
    pub fn from_source(source: &CStr, kind: gl::types::GLenum) -> Result<Shader, String> {
        let id = shader_from_source(source, kind)?;
        Ok(Shader { id })
    }

    pub fn from_vert_source(source: &CStr) -> Result<Shader, String> {
        Shader::from_source(source, gl::VERTEX_SHADER)
    }

    pub fn from_frag_source(source: &CStr) -> Result<Shader, String> {
        Shader::from_source(source, gl::FRAGMENT_SHADER)
    }
}

// Destructor
impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}

pub struct Program {
    pub id: gl::types::GLuint,
}

impl Program {
    pub fn from_shaders(shaders: &[Shader]) -> Result<Program, String> {
        let program_id = unsafe { gl::CreateProgram() };

        for shader in shaders {
            unsafe { gl::AttachShader(program_id, shader.id); }
        }

        unsafe { gl::LinkProgram(program_id); }

        let mut success: gl::types::GLint = 1;
        unsafe {
            gl::GetProgramiv(program_id, gl::LINK_STATUS, &mut success);
        }

        if success == 0 {
            let mut len: gl::types::GLint = 0;
            unsafe {
                gl::GetProgramiv(program_id, gl::INFO_LOG_LENGTH, &mut len);
            }

            let error = create_whitespace_cstring_with_len(len as usize);

            unsafe {
                gl::GetProgramInfoLog(
                    program_id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut gl::types::GLchar
                );
            }

            return Err(error.to_string_lossy().into_owned());
        }

        for shader in shaders {
            unsafe { gl::DetachShader(program_id, shader.id); }
        }

        Ok(Program { id: program_id })
    }

    pub fn set_used(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}

fn create_whitespace_cstring_with_len(len: usize) -> CString {
    // allocate buffer of correct size
    let mut buffer: Vec<u8> = Vec::with_capacity(len + 1);
    // fill it with len spaces
    buffer.extend([b' '].iter().cycle().take(len));
    // convert buffer to CString
    unsafe { CString::from_vec_unchecked(buffer) }
}

// Shader stuff
fn shader_from_source(source: &CStr, kind: gl::types::GLuint) -> Result<gl::types::GLuint, String> {
    let id = unsafe { gl::CreateShader(kind) };
    unsafe {
        gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
        gl::CompileShader(id);
    }

    let mut success: gl::types::GLint = 1;
    unsafe {
        gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
    }

    if success == 0 {
        // Get the required length of the error string
        let mut len: gl::types::GLint = 0;
        unsafe {
            gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
        }
        // allocate buffer of correct size
        let mut buffer: Vec<u8> = Vec::with_capacity(len as usize + 1);
        // fill it with len spaces
        buffer.extend([b' '].iter().cycle().take(len as usize));
        // convert buffer to CString
        let error = create_whitespace_cstring_with_len(len as usize);
        unsafe {
            gl::GetShaderInfoLog(
                id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut gl::types::GLchar
            );
        }
        return Err(error.to_string_lossy().into_owned());
    }

    Ok(id)
}
