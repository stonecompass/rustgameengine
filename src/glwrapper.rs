use gl;
use std::ffi::CString;
use cgmath::{Matrix4};
use cgmath::prelude::*;
use renderer::{Color};

pub fn gl_viewport(x: i32, y: i32, width: i32, height: i32) {
    unsafe { gl::Viewport(x, y, width, height); }
}

pub fn gl_clear_color(color: Color) {
    match color {
        Color::Rgba(r, g, b, a) => {
            unsafe { gl::ClearColor(r, g, b, a); }
        },
        Color::Rgb(r, g, b) => {
            unsafe { gl::ClearColor(r, g, b, 1.0); }
        }
    }
}

pub fn gl_clear(bit: gl::types::GLenum) {
    unsafe { gl::Clear(bit); }
}

pub fn gl_bind_vertex_array(vao: gl::types::GLuint) {
    unsafe { gl::BindVertexArray(vao); }
}

pub fn gl_bind_buffer(buffer_type: gl::types::GLenum, buffer: gl::types::GLuint) {
    unsafe { gl::BindBuffer(buffer_type, buffer); }
}

pub fn gl_buffer_data() {
}

pub fn gl_draw_arrays(draw_type: gl::types::GLenum, first: gl::types::GLint, count: gl::types::GLsizei) {
    unsafe { gl::DrawArrays(draw_type, first, count); }
}

pub fn gl_uniform2f(location: gl::types::GLint, x: gl::types::GLfloat, y: gl::types::GLfloat) {
    unsafe { gl::Uniform2f(location, x, y); }
}

pub fn gl_uniform3f(location: gl::types::GLint, x: gl::types::GLfloat, y: gl::types::GLfloat, z: gl::types::GLfloat) {
    unsafe { gl::Uniform3f(location, x, y, z); }
}

pub fn gl_uniform4f(location: gl::types::GLint, x: gl::types::GLfloat, y: gl::types::GLfloat, z: gl::types::GLfloat, w: gl::types::GLfloat) {
    unsafe { gl::Uniform4f(location, x, y, z, w); }
}

pub fn gl_uniform_mat4fv(location: gl::types::GLint, matrix: Matrix4<f32>) {
    unsafe { gl::UniformMatrix4fv(location, 1, gl::FALSE, matrix.as_ptr()); }
}

pub fn gl_get_uniform_location(program: gl::types::GLuint, name: &str) -> i32 {
    let c_string = CString::new(name).unwrap();
    gl_get_uniform_location_internal(program, c_string.as_ptr())
}

fn gl_get_uniform_location_internal(program: gl::types::GLuint, name: *const gl::types::GLchar) -> i32 {
    unsafe { gl::GetUniformLocation(program, name) }
}

pub fn gl_gen_buffers(count: i32, buffer: &mut gl::types::GLuint) {
    unsafe { gl::GenBuffers(count, buffer); }
}
