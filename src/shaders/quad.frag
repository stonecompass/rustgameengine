#version 330 core
uniform sampler2D tex;

in vec2 uv;
in vec4 frag_color;
out vec4 out_color;

void main()
{
    vec4 tex_color = texture(tex, uv);
    out_color = tex_color * frag_color;
}
