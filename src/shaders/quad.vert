#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 color;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec2 tex_coord;

out vec2 uv;
out vec4 frag_color;

void main() {
    gl_Position = projection * view * model * vec4(vertex.xyz, 1.0);
    uv = tex_coord;
    frag_color = color;
}
