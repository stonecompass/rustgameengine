#version 330 core
uniform sampler2D tex;

uniform vec2 texture_size;
uniform vec2 frame_size;
uniform vec2 sprite_position;

in vec2 uv;
in vec4 frag_color;
out vec4 out_color;

void main()
{
    // Calculate the correct texture coordinates
    vec2 real_uv  = vec2(1.0 / texture_size.x * sprite_position.x + uv.x / (texture_size.x / frame_size.x), 1.0 / texture_size.y * sprite_position.y + uv.y / (texture_size.y / frame_size.y));
    vec4 tex_color = texture(tex, real_uv);

    if(tex_color.a == 0.0)
    {
        discard;
    }

    out_color = tex_color * frag_color;
}
