extern crate sdl2;
extern crate gl;
extern crate cgmath;
extern crate stb_image;

pub mod renderer;
pub mod gamestate;
pub mod glwrapper;

use glitchheart::{Engine, GraphicsApi};
use gamestate::{GameState};

fn main() {
    if let Ok(mut engine) = Engine::init(GraphicsApi::OpenGL) {
        let mut game_state = GameState::create();
        game_state.initialize(&mut engine.renderer);

        engine.run(&mut |dt, mut engine| {
            game_state.update(dt, &mut engine);
        });
    }
}
