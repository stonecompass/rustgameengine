use renderer::{Renderer, Color, Texture, Animation, AnimationFrame, AnimationHandle};
use glitchheart::{Engine, Key};

struct Position {
    x: f32,
    y: f32
}

pub struct GameState {
    texture: Texture,
    animation_handle: AnimationHandle,
    positions: Vec<Position>
}

impl GameState {
    pub fn create() -> GameState {
        let mut pos_vec = Vec::new();
        pos_vec.push(Position { x: 500.0, y: 500.0 });

        GameState {
            texture: Texture { id: 0, width: 0, height: 0 },
            animation_handle: AnimationHandle { handle: 0 },
            positions: pos_vec
        }
    }

    pub fn initialize(&mut self, renderer: &mut Renderer) {
        match Renderer::load_texture("character.png") {
            Ok(texture) => {
                self.texture = texture;
                let mut animation = Animation::new(String::from("test_animation"), texture, true, true);

                let frame_count = 11;
                for i in 0..frame_count {
                    animation.add_frame(AnimationFrame::new(i * 128, 0, 128, 128, i as f64 * 0.01));
                }

                self.animation_handle = renderer.register_animation(animation);
            },
            Err(message) => {
                println!("{}", message);
            }
        }
    }

    pub fn update(&mut self, dt: f64, engine: &mut Engine) {
        // Check if we should close the application
        if engine.input_state.key_down(Key::Escape) {
            engine.exit();
        }

        let input_state = &engine.input_state;
        let renderer = &mut engine.renderer;

        if input_state.key_down(Key::Up) {
            self.positions[0].y = self.positions[0].y + 200.0 * dt as f32;
        } else if input_state.key_down(Key::Down) {
            self.positions[0].y = self.positions[0].y - 200.0 * dt as f32;
        }

        if input_state.key_down(Key::Left) {
            self.positions[0].x = self.positions[0].x - 200.0 * dt as f32;
        } else if input_state.key_down(Key::Right) {
            self.positions[0].x = self.positions[0].x + 200.0 * dt as f32;
        }

        renderer.camera.update_position(-250.0, -250.0, 0.0);
        renderer.clear_color = Color::Rgba(0.0, 0.0, 0.0, 1.0);

        for pos in &self.positions {
            renderer.push_animated_sprite_command(self.animation_handle, pos.x, pos.y, 512.0, 512.0, [1.0, 1.0, 1.0, 1.0]);
        }
    }
}
